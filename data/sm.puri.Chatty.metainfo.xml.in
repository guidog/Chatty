<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
    <id>sm.puri.Chatty</id>
    <metadata_license>CC0-1.0</metadata_license>
    <project_license>GPL-3.0-or-later</project_license>
    <name>Chats</name>
    <summary>Messaging application for mobile and desktop</summary>
    <description>
      <p>Chats is a simple to use to messaging app for 1:1 communication and small groups supporting SMS, MMS, XMPP and Matrix.</p>
    </description>
    <url type="homepage">https://source.puri.sm/Librem5/chatty</url>
    <translation type="gettext">purism-chatty</translation>
    <update_contact>librem5-dev@lists.community.puri.sm</update_contact>
    <screenshots>
        <screenshot type="default">
            <caption>Chats message window</caption>
            <image type="source" width="906" height="621">https://source.puri.sm/Librem5/chatty/-/raw/master/data/screenshots/screenshot.png?inline=false</image>
        </screenshot>
    </screenshots>
    <mimetypes>
        <mimetype>x-scheme-handler/sms</mimetype>
    </mimetypes>
    <content_rating type="oars-1.1">
        <content_attribute id="social-chat">intense</content_attribute>
    </content_rating>
    <releases>
      <release version="0.7.3" date="2023-05-13">
        <description>
          <ul>
            <li>Fix some memory leaks</li>
            <li>Improve sidebar</li>
            <li>Fix logging stack traces on crash</li>
            <li>Fix message counter for XMPP messages</li>
            <li>Update Translations</li>
          </ul>
        </description>
      </release>
      <!-- todo: Add past releases -->
    </releases>
    <requires>
      <display_length>360</display_length>
    </requires>
    <supports>
      <control>keyboard</control>
      <control>pointing</control>
      <control>touch</control>
    </supports>
    <!-- todo: drop this for crimson -->
    <custom>
      <value key="Purism::form_factor">workstation</value>
      <value key="Purism::form_factor">mobile</value>
    </custom>
</component>
