/*
 * Copyright 2023, Purism SPC
 *           2023, Chris Talbot
 *
 * Author(s):
 *   Chris Talbot
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <gtk/gtk.h>
#include <gtk/gtkwidget.h>

void        *chatty_mm_notify_message (const char          *title,
                                       const char          *body);
