/*
 * Author: Mohammed Sadiq <www.sadiqpk.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later OR CC0-1.0
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

int         gtk_dialog_run                (GtkDialog       *dialog);

G_END_DECLS
